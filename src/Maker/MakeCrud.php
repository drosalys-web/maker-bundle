<?php

/*
 * This file is part of the lifiachan package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\MakerBundle\Maker;

use Doctrine\Inflector\Inflector;
use Doctrine\Inflector\InflectorFactory;
use Doctrine\ORM\Mapping\ClassMetadata;
use Drosalys\Bundle\MakerBundle\Doctrine\DoctrineHelper;
use Drosalys\Bundle\MakerBundle\Renderer\FilterTypeRenderer;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Maker\MakeCrud as BaseMake;
use Symfony\Bundle\MakerBundle\Renderer\FormTypeRenderer;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Class MakeCrud
 *
 * @author Benjamin Georgeault
 */
final class MakeCrud extends AbstractMaker
{
    private Inflector $inflector;
    private string $actionDirectoryName;

    public function __construct(
        private BaseMake $symfonyMakeCrud,
        private FormTypeRenderer $formTypeRenderer,
        private FilterTypeRenderer $filterRenderer,
        private DoctrineHelper $doctrineHelper,
    ) {
        $this->inflector = InflectorFactory::create()->build();
    }

    public static function getCommandName(): string
    {
        return 'make:crud';
    }

    public static function getCommandDescription(): string
    {
        return 'Creates CRUD for Doctrine entity class';
    }

    public function configureCommand(Command $command, InputConfiguration $inputConfig)
    {
        $command
            ->addArgument('entity-class', InputArgument::OPTIONAL, sprintf('The class name of the entity to create CRUD (e.g. <fg=yellow>%s</>)', Str::asClassName(Str::getRandomTerm())))
            ->setHelp(file_get_contents(__DIR__.'/../Resources/help/MakeCrud.txt'))
        ;

        $inputConfig->setArgumentAsNonInteractive('entity-class');
    }

    public function interact(InputInterface $input, ConsoleStyle $io, Command $command)
    {
        if (null === $input->getArgument('entity-class')) {
            $argument = $command->getDefinition()->getArgument('entity-class');
            $question = new Question($argument->getDescription());
            $question->setAutocompleterValues($this->doctrineHelper->getEntitiesForAutocomplete());
            $value = $io->askQuestion($question);

            $input->setArgument('entity-class', $value);
        }

        $defaultActionDirectoryName = Str::asClassName(sprintf('%s', $input->getArgument('entity-class')));

        $this->actionDirectoryName = $io->ask(
            sprintf('Choose a name for your action class\'s directory name (e.g. <fg=yellow>%s</>)', $defaultActionDirectoryName),
            $defaultActionDirectoryName,
        );
    }

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        $entityClassDetails = $generator->createClassNameDetails(
            Validator::entityExists($input->getArgument('entity-class'), $this->doctrineHelper->getEntitiesForAutocomplete()),
            'Entity\\',
        );

        $metadata = $this->doctrineHelper->getMetadata($entityClassDetails->getFullName());

        try {
            $vars = array_merge(
                [
                    'entity_full_class_name' => $entityClassDetails->getFullName(),
                    'entity_class_name' => $entityClassDetails->getShortName(),
                ],
                $this->getDoctrineVars($metadata, $generator),
                $this->getFormVars($entityClassDetails, $generator),
                $this->getFilterVars($entityClassDetails, $generator),
                $this->getGlobalVars($entityClassDetails),
                $this->getRouteAndTemplateVars($generator),
            );
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            return;
        }

        $this->generateAction('index', $generator, $vars);
        $this->generateAction('new', $generator, $vars);
        $this->generateAction('show', $generator, $vars);
        $this->generateAction('edit', $generator, $vars);
        $this->generateAction('delete', $generator, $vars, false);

        foreach (['_form', '_delete_form'] as $staticTemplate) {
            $generator->generateTemplate(
                $vars['templates_path'] . '/' . $staticTemplate . '.html.twig',
                'crud/templates/' . $staticTemplate . '.tpl.php',
                $vars,
            );
        }

        $generator->writeChanges();

        $this->writeSuccessMessage($io);
    }

    public function configureDependencies(DependencyBuilder $dependencies)
    {
        $this->symfonyMakeCrud->configureDependencies($dependencies);

        $dependencies->addClassDependency(
            LexikFormFilterBundle::class,
            'lexik/form-filter-bundle',
        );

        $dependencies->addClassDependency(
            KnpPaginatorBundle::class,
            'knplabs/knp-paginator-bundle',
        );
    }

    private function generateAction(string $action, Generator $generator, array $vars, bool $hasTemplate = true): void
    {
        $collectionClassDetails = $generator->createClassNameDetails(
            $this->actionDirectoryName.'\\'.ucfirst($action),
            'Controller\\',
            'Action',
        );

        $generator->generateController(
            $collectionClassDetails->getFullName(),
            'crud/controller/'.$action.'.tpl.php',
            $vars,
        );

        if ($hasTemplate) {
            $generator->generateTemplate(
                $vars['templates_path'] . '/' . $action . '.html.twig',
                'crud/templates/' . $action . '.tpl.php',
                $vars,
            );
        }
    }

    private function getDoctrineVars(ClassMetadata $metadata, Generator $generator): array
    {
        if (null === $repositoryClass = $metadata->customRepositoryClassName) {
            throw new \LogicException(sprintf(
                'To use drosalys make:crud, your entity "%s" need to have custom repository.',
                $metadata->getName(),
            ));
        }

        $repositoryClassDetails = $generator->createClassNameDetails(
            '\\'.$repositoryClass,
            'Repository\\',
            'Repository',
        );

        return [
            'entity_identifier' => $metadata->getIdentifier()[0],
            'entity_fields' => $metadata->fieldMappings,
            'repository_full_class_name' => $repositoryClassDetails->getFullName(),
            'repository_class_name' => $repositoryClassDetails->getShortName(),
            'repository_var' => lcfirst($this->inflector->singularize($repositoryClassDetails->getShortName())),
        ];
    }

    private function getFormVars(ClassNameDetails $entityClassDetails, Generator $generator): array
    {
        $formClassDetails = $generator->createClassNameDetails(
            $entityClassDetails->getRelativeNameWithoutSuffix().'Type',
            'Form\\',
            'Type',
        );

        if (!class_exists($formClassDetails->getFullName())) {
            $this->formTypeRenderer->render(
                $formClassDetails,
                $this->doctrineHelper->getFormFields($entityClassDetails->getFullName()),
                $entityClassDetails,
            );
        }

        return [
            'form_full_class_name' => $formClassDetails->getFullName(),
            'form_class_name' => $formClassDetails->getShortName(),
        ];
    }

    private function getFilterVars(ClassNameDetails $entityClassDetails, Generator $generator): array
    {
        $filterClassNameDetails = $generator->createClassNameDetails(
            $entityClassDetails->getRelativeNameWithoutSuffix(),
            'Filter\\',
            'Filter',
        );

        if (!class_exists($filterClassNameDetails->getFullName())) {
            $filterFields = $this->doctrineHelper->getFilterFields($entityClassDetails->getFullName());

            $this->filterRenderer->render(
                $filterClassNameDetails,
                $filterFields,
                true,
            );
        }

        return [
            'filter_full_class_name' => $filterClassNameDetails->getFullName(),
            'filter_class_name' => $filterClassNameDetails->getShortName(),
        ];
    }

    private function getGlobalVars(ClassNameDetails $entityClassDetails): array
    {
        $entityVarPlural = lcfirst($this->inflector->pluralize($entityClassDetails->getShortName()));
        $entityVarSingular = lcfirst($this->inflector->singularize($entityClassDetails->getShortName()));

        return [
            'entity_var_plural' => $entityVarPlural,
            'entity_twig_var_plural' => Str::asTwigVariable($entityVarPlural),
            'entity_var_singular' => $entityVarSingular,
            'entity_twig_var_singular' => Str::asTwigVariable($entityVarSingular),
        ];
    }

    private function getRouteAndTemplateVars(Generator $generator): array
    {
        // Only used to generation route and template variables.
        $oldClassDetails = $generator->createClassNameDetails(
            $this->actionDirectoryName,
            'Controller\\',
            'Action',
        );

        return [
            'route_path' => Str::asRoutePath($oldClassDetails->getRelativeNameWithoutSuffix()),
            'route_name' => Str::asRouteName($oldClassDetails->getRelativeNameWithoutSuffix()),
            'templates_path' => Str::asFilePath($oldClassDetails->getRelativeNameWithoutSuffix()),
        ];
    }
}
