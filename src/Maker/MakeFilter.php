<?php

/*
 * This file is part of the lifiachan package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\MakerBundle\Maker;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Drosalys\Bundle\MakerBundle\Doctrine\DoctrineHelper;
use Drosalys\Bundle\MakerBundle\Renderer\FilterTypeRenderer;
use Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Validation;

/**
 * Class MakeFilter
 *
 * @author Benjamin Georgeault
 *
 * @see \Symfony\Bundle\MakerBundle\Maker\MakeForm
 */
final class MakeFilter extends AbstractMaker
{
    public function __construct(
        private DoctrineHelper $doctrineHelper,
        private FilterTypeRenderer $renderer,
    ) {}

    public static function getCommandName(): string
    {
        return 'make:filter';
    }

    public static function getCommandDescription(): string
    {
        return 'Create a new form type use to filter query via lexik/form-filter-bundle.';
    }

    public function configureCommand(Command $command, InputConfiguration $inputConfig)
    {
        $command
            ->addArgument('name', InputArgument::OPTIONAL, sprintf('The name of the filter class (e.g. <fg=yellow>%sFilter</>)', Str::asClassName(Str::getRandomTerm())))
            ->addArgument('entity-class', InputArgument::OPTIONAL, 'The name of Entity or fully qualified model class name that the new form will filter')
            ->setHelp(file_get_contents(__DIR__.'/../Resources/help/MakeFilter.txt'))
        ;

        $inputConfig->setArgumentAsNonInteractive('entity-class');
    }

    public function configureDependencies(DependencyBuilder $dependencies)
    {
        $dependencies->addClassDependency(
            AbstractType::class,
            'form',
        );

        $dependencies->addClassDependency(
            LexikFormFilterBundle::class,
            'lexik/form-filter-bundle',
        );

        $dependencies->addClassDependency(
            Validation::class,
            'validator',
            // add as an optional dependency: the user *probably* wants validation
            false,
        );

        $dependencies->addClassDependency(
            DoctrineBundle::class,
            'orm',
            false,
        );
    }

    public function interact(InputInterface $input, ConsoleStyle $io, Command $command)
    {
        if (null === $input->getArgument('entity-class')) {
            $argument = $command->getDefinition()->getArgument('entity-class');

            $entities = $this->doctrineHelper->getEntitiesForAutocomplete();

            $question = new Question($argument->getDescription());
            $question->setValidator(function ($answer) use ($entities) {return Validator::existsOrNull($answer, $entities);});
            $question->setAutocompleterValues($entities);
            $question->setMaxAttempts(3);

            $input->setArgument('entity-class', $io->askQuestion($question));
        }
    }

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        $filterClassNameDetails = $generator->createClassNameDetails(
            $input->getArgument('name'),
            'Filter\\',
            'Filter',
        );

        if (null !== $boundClass = $input->getArgument('entity-class')) {
            $boundClassDetails = $generator->createClassNameDetails(
                $boundClass,
                'Entity\\',
            );

            $filterFields = $this->doctrineHelper->getFilterFields($boundClassDetails->getFullName());
        }

        $this->renderer->render(
            $filterClassNameDetails,
            $filterFields ?? [],
        );

        $generator->writeChanges();

        $this->writeSuccessMessage($io);

        $io->text([
            'Next: Add fields to your form filter and start using it.',
            'Find the form\'s documentation at <fg=yellow>https://symfony.com/doc/current/forms.html</>',
            'Find the lexik form filter\'s documentation at <fg=yellow>https://github.com/lexik/LexikFormFilterBundle/</>',
        ]);
    }
}
