<?php

/*
 * This file is part of the lifiachan package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\MakerBundle\DependencyInjection;

use Drosalys\Bundle\MakerBundle\DecorateGenerator;
use Drosalys\Bundle\MakerBundle\Doctrine\DoctrineHelper;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class DrosalysMakerExtension
 *
 * @author Benjamin Georgeault
 */
class DrosalysMakerExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        $this->skeletonDirectories($config, $container);
        $this->doctrine($config, $container);
    }

    private function skeletonDirectories(array $config, ContainerBuilder $container): void
    {
        $container->getDefinition(DecorateGenerator::class)
            ->setArgument(1, $config['skeleton_directories'])
        ;
    }

    private function doctrine(array $config, ContainerBuilder $container): void
    {
        $container->getDefinition(DoctrineHelper::class)
            ->setArgument(0, $config['root_namespace'].'\\Entity')
        ;
    }
}
