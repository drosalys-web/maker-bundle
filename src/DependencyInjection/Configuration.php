<?php

/*
 * This file is part of the lifiachan package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\MakerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        ($treeBuilder = new TreeBuilder('drosalys_maker'))->getRootNode()
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('skeleton_directories')
                    ->beforeNormalization()
                        ->castToArray()
                    ->end()
                    ->scalarPrototype()->end()
                ->end()
                ->scalarNode('root_namespace')->defaultValue('App')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
