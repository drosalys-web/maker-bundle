<?= "<?php\n" ?>

namespace <?= $namespace ?>;

use <?= $entity_full_class_name ?>;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\<?= $parent_class_name ?>;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class <?= $class_name ?> extends <?= $parent_class_name; ?><?= "\n" ?>
{
    public function __construct(
        private EntityManagerInterface $em,
    ) {}

    #[Route('<?= $route_path ?>/{<?= $entity_identifier ?>}/delete', name: '<?= $route_name ?>_delete', methods: ['POST'])]
    public function __invoke(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>): Response
    {
        if ($this->isCsrfTokenValid('delete'.$<?= $entity_var_singular ?>->get<?= ucfirst($entity_identifier) ?>(), $request->request->get('_token'))) {
            $this->em->remove($<?= $entity_var_singular ?>);
            $this->em->flush();
        }

        return $this->redirectToRoute('<?= $route_name ?>_index', [], Response::HTTP_SEE_OTHER);
    }
}
