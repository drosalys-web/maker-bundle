<?= "<?php\n" ?>

namespace <?= $namespace ?>;

use <?= $filter_full_class_name ?>;
use <?= $repository_full_class_name ?>;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\<?= $parent_class_name ?>;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class <?= $class_name ?> extends <?= $parent_class_name; ?><?= "\n" ?>
{
    public function __construct(
        private PaginatorInterface $paginator,
        private <?= $repository_class_name ?> $<?= $repository_var ?>,
        private FilterBuilderUpdaterInterface $builderUpdater,
        private FormFactoryInterface $formFactory,
    ) {}

    #[Route('<?= $route_path ?>', name: '<?= $route_name ?>_index', methods: ['GET'])]
    public function __invoke(Request $request): Response
    {
        $filter = $this->formFactory->createNamed('f', <?= $filter_class_name ?>::class);
        $filter->handleRequest($request);
        $this->builderUpdater->addFilterConditions($filter, $qb = $this-><?= $repository_var ?>->queryAll());

        $<?= $entity_var_plural ?> = $this->paginator->paginate($qb, $request->query->getInt('page', 1), 20);

        return $this->render('<?= $templates_path ?>/index.html.twig', [
            '<?= $entity_twig_var_plural ?>' => $<?= $entity_var_plural ?>,
        ]);
    }
}
