<?php

/*
 * This file is part of the lifiachan package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\MakerBundle\Renderer;

use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;

/**
 * Class FilterTypeRenderer
 *
 * @author Benjamin Georgeault
 */
final class FilterTypeRenderer
{
    public function __construct(
        private Generator $generator,
    ) {}

    public function render(ClassNameDetails $formClassDetails, array $formFields, bool $forceWrite = false): void
    {
        $fieldTypeUseStatements = [];
        $fields = [];
        foreach ($formFields as $name => $fieldTypeOptions) {
            $fieldTypeOptions = $fieldTypeOptions ?? ['type' => null, 'options_code' => null];

            if (isset($fieldTypeOptions['type'])) {
                $fieldTypeUseStatements[] = $fieldTypeOptions['type'];
                $fieldTypeOptions['type'] = Str::getShortClassName($fieldTypeOptions['type']);
            }

            $fields[$name] = $fieldTypeOptions;

            if (str_contains($fieldTypeOptions['options_code'] ?? '', 'FilterOperands::')) {
                $fieldTypeUseStatements[] = FilterOperands::class;
            }
        }

        $mergedTypeUseStatements = array_unique($fieldTypeUseStatements);
        sort($mergedTypeUseStatements);

        $this->generator->generateClass(
            $formClassDetails->getFullName(),
            'filter/Filter.tpl.php',
            [
                'form_fields' => $fields,
                'field_type_use_statements' => $mergedTypeUseStatements,
            ]
        );

        if ($forceWrite) {
            $this->generator->writeChanges();
        }
    }
}
