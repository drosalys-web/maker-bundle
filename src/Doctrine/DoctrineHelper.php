<?php

/*
 * This file is part of the lifiachan package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\MakerBundle\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\Persistence\ManagerRegistry;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\BooleanFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateRangeFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateTimeRangeFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\NumberFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\NumberRangeFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;

/**
 * Class DoctrineHelper
 *
 * @author Benjamin Georgeault
 */
class DoctrineHelper
{
    public function __construct(
        private string $entityNamespace,
        private ManagerRegistry $registry,
    ) {}

    /**
     * @return string[]
     */
    public function getEntitiesForAutocomplete(): array
    {
        $entities = [];

        foreach ($this->getMetadata() as $metadata) {
            $entityClassDetails = new ClassNameDetails($metadata->getName(), $this->entityNamespace);
            $entities[] = $entityClassDetails->getRelativeName();
        }

        sort($entities);

        return $entities;
    }

    /**
     * @return ClassMetadata[]|ClassMetadata
     */
    public function getMetadata(?string $classOrNamespace = null): array|ClassMetadata
    {
        $metadata = [];

        foreach ($this->registry->getManagers() as $em) {
            $cmf = $em->getMetadataFactory();

            foreach ($cmf->getAllMetadata() as $m) {
                if (null === $classOrNamespace) {
                    $metadata[$m->getName()] = $m;
                } elseif ($m->getName() === $classOrNamespace) {
                    return $m;
                } elseif (str_starts_with($m->getName(), $classOrNamespace)) {
                    $metadata[$m->getName()] = $m;
                }
            }
        }

        return $metadata;
    }

    public function getFilterFields(string $classOrNamespace): array
    {
        $metadata = $this->getMetadata($classOrNamespace);

        $fields = [];

        foreach ($metadata->getFieldNames() as $field) {
            $fields[$field] = $this->convertDoctrineTypeToFilterType($metadata->getTypeOfField($field), $metadata->isIdentifier($field));
        }

        return $fields;
    }

    public function getFormFields(string $classOrNamespace): array
    {
        $metadata = $this->getMetadata($classOrNamespace);
        
        $fields = (array) $metadata->fieldNames;
        // Remove the primary key field if it's not managed manually
        if (!$metadata->isIdentifierNatural()) {
            $fields = array_diff($fields, $metadata->identifier);
        }
        $fields = array_values($fields);

        if (!empty($metadata->embeddedClasses)) {
            foreach (array_keys($metadata->embeddedClasses) as $embeddedClassKey) {
                $fields = array_filter($fields, function ($v) use ($embeddedClassKey) {
                    return !str_starts_with($v, $embeddedClassKey . '.');
                });
            }
        }

        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if (ClassMetadataInfo::ONE_TO_MANY !== $relation['type']) {
                $fields[] = $fieldName;
            }
        }

        $fieldsWithTypes = [];
        foreach ($fields as $field) {
            $fieldsWithTypes[$field] = null;
        }

        return $fieldsWithTypes;
    }

    private function convertDoctrineTypeToFilterType(string $type, bool $isId = false): array
    {
        $finalType = null;
        $options = null;
        
        if ('integer' === $type) {
            $finalType = $isId ? NumberFilterType::class : NumberRangeFilterType::class;
        } elseif ('string' === $type) {
            $finalType = TextFilterType::class;
            $options = '                \'condition_pattern\' => FilterOperands::STRING_CONTAINS,';
        } elseif ('boolean' === $type) {
            $finalType = BooleanFilterType::class;
        } elseif ('date' === $type) {
            $finalType = DateRangeFilterType::class;
        } elseif ('datetime' === $type) {
            $finalType = DateTimeRangeFilterType::class;
        }

        return [
            'type' => $finalType,
            'options_code' => $options,
        ];
    }
}
