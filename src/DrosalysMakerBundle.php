<?php

/*
 * This file is part of the lifiachan package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\MakerBundle;

use Symfony\Bundle\MakerBundle\Doctrine\DoctrineHelper;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DrosalysMakerBundle
 *
 * @author Benjamin Georgeault
 */
class DrosalysMakerBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        if ('dev' !== $container->getParameter('kernel.environment')) {
            throw new \RuntimeException('DrosalysMakerBundle should only be enable on DEV environment.');
        }

        // Check internal classes from Maker to alert on update from the bundle.
        if (!class_exists(DoctrineHelper::class)) {
            throw new ServiceNotFoundException('maker.doctrine_helper', msg: sprintf(
                'Class "%s" missing. Maybe it has been removed from symfony/maker-bundle.',
                DoctrineHelper::class,
            ));
        }

        if (!class_exists(Validator::class)) {
            throw new LogicException('maker.maker.make_crud', msg: sprintf(
                'Class "%s" missing. Maybe it has been removed from symfony/maker-bundle.',
                Validator::class,
            ));
        }
    }
}
